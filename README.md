# Angular Propotype for China RSA Team

This project is to build for training, its propose is to get the main point to cut into our new project.

## Build & development

Run `npm install` to install all node components. 

Run `bower install` to install all js/css components.

Run `grunt serve` to open http port to monitor.

Run `grunt build` to compile the project and generate the dist file.

## Testing

Running `grunt test` will run the unit tests with karma.